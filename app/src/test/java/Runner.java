import org.browser.Web;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
public class Runner {
    private WebDriver driver=null;
    @BeforeSuite
    public void initializeDriver()
    {
        Web web=new Web();
        driver=web.chromeDrvier();
    }
    @Test
    public void start()
    {
        driver.get("https://about.gitlab.com/");
        String titile=driver.getTitle();
        System.out.println(titile);
        Assert.assertEquals(titile,"The most-comprehensive AI-powered DevSecOps platform | GitLab");
    }
    @AfterTest
    public void quitDriver()
    {
        driver.quit();
    }
}
