package org.browser;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Web {
    public WebDriver chromeDrvier()  {
        //System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options=new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        WebDriver driver=new ChromeDriver(options);
        //WebDriver driver = WebDriverManager.chromedriver().create();
        //WebDriver driver=new ChromeDriver();
        return driver;
    }

}

